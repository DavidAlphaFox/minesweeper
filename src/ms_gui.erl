%%% @doc
%%% Minesweeper GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(ms_gui).
-vsn("1.0.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-export([new_grid/1, mine_total/1, update/1, you_died/0, you_won/0]).
-export([start_link/1]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {frame      = none      :: none | wx:wx_object(),
         panel      = none      :: none | wx:wx_object(),
         start_ts   = none      :: erlang:timestamp(),
         squares    = []        :: [square()],
         size       = {10,  10} :: {integer(), integer()},
         mine_total = 0         :: integer(),
         flag_count = 0         :: integer(),
         timer      = none      :: none | reference(),
         unclicked              :: wx:wxBitmap(),
         empty                  :: wx:wxBitmap(),
         marked                 :: wx:wxBitmap(),
         maybe                  :: wx:wxBitmap(),
         mine                   :: wx:wxBitmap(),
         assplode               :: wx:wxBitmap(),
         adjacent               :: tuple()}).

-record(square,
        {coord  = {0, 0}    :: {integer(), integer()},
         status = unclicked :: unclicked | marked | maybe | mine | {adjacent, 1..8}}).


-type state()  :: #s{}.
-type square() :: #square{}.


-define(gameNEW, 11).
-define(gameETC, 12).



%%% Interface functions

new_grid({X, Y}) ->
    wx_object:cast(?MODULE, {new_grid, X, Y}).


mine_total(Count) ->
    wx_object:cast(?MODULE, {mine_total, Count}).


update(Board) ->
    wx_object:cast(?MODULE, {update, Board}).


you_died() ->
    wx_object:cast(?MODULE, you_died).


you_won() ->
    wx_object:cast(?MODULE, you_won).


%%% Startup Functions

start_link(Title) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, Title, []).


init(Title) ->
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Title),

    MB = wxMenuBar:new(),
    Game = wxMenu:new([]),
    Help = wxMenu:new([]),
    _ = wxMenu:append(Game, ?gameNEW, "New Game"),
    _ = wxMenu:append(Game, ?gameETC, "Settings"),
    _ = wxMenu:append(Game, ?wxID_EXIT, "&Quit"),
    _ = wxMenu:append(Help, ?wxID_ABOUT, "About"),
    _ = wxMenuBar:append(MB, Game, "&Game"),
    _ = wxMenuBar:append(MB, Help, "&Help"),
    ok = wxFrame:setMenuBar(Frame, MB),

    _ = wxFrame:createStatusBar(Frame),
    ok = wxFrame:setStatusText(Frame, "000 / 000  |  000.000"),

    Panel = wxPanel:new(Frame),
    BoardPX = {300, 300},
    ok = wxPanel:setSize(Panel, BoardPX),
    ok = wxPanel:connect(Panel, paint),
    ok = wxPanel:connect(Panel, left_up),
    ok = wxPanel:connect(Panel, right_up),

    ImgDir = filename:join([zx_daemon:get_home(), "theme", "icons"]),
    PNG = [{type, ?wxBITMAP_TYPE_PNG}],
    Unclicked = wxBitmap:new(filename:join(ImgDir, "unclicked.png"), PNG),
    Empty     = wxBitmap:new(filename:join(ImgDir, "empty.png"), PNG),
    Marked    = wxBitmap:new(filename:join(ImgDir, "marked.png"), PNG),
    Maybe     = wxBitmap:new(filename:join(ImgDir, "maybe.png"), PNG),
    Mine      = wxBitmap:new(filename:join(ImgDir, "mine.png"), PNG),
    Assplode  = wxBitmap:new(filename:join(ImgDir, "assplode.png"), PNG),
    Adjacent  = load_adjacent(ImgDir, PNG),

    MainSz = wxBoxSizer:new(?wxVERTICAL),
    _ = wxSizer:add(MainSz, Panel, [{flag, ?wxEXPAND}, {proportion, 1}]),
    ok = wxFrame:setSizer(Frame, MainSz),
    ok = wxSizer:layout(MainSz),

    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:connect(Frame, command_menu_selected),
    ok = wxFrame:setClientSize(Frame, BoardPX),
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),
    State = #s{frame     = Frame,
               panel     = Panel,
               unclicked = Unclicked,
               empty     = Empty,
               marked    = Marked,
               maybe     = Maybe,
               mine      = Mine,
               assplode  = Assplode,
               adjacent  = Adjacent},
    {Frame, State}.


load_adjacent(ImgDir, Type) ->
    Load = fun(File) -> wxBitmap:new(filename:join(ImgDir, File), Type) end,
    list_to_tuple([Load(integer_to_list(N) ++ ".png") || N <- lists:seq(1, 8)]).



%%% wx_object

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast({update, Board}, State) ->
    NewState = do_update(Board, State),
    {noreply, NewState};
handle_cast({new_grid, X, Y}, State) ->
    NewState = do_new_grid(X, Y, State),
    {noreply, NewState};
handle_cast({mine_total, Count}, State) ->
    NewState = do_mine_total(Count, State),
    {noreply, NewState};
handle_cast(you_died, State) ->
    NewState = funeral(State),
    {noreply, NewState};
handle_cast(you_won, State) ->
    NewState = party(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(time_hack, State) ->
    NewState = do_time_hack(State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_event(#wx{event = #wxMouse{type = Type, x = X, y = Y}},
             State = #s{frame = Frame, size = {Cols, Rows}}) ->
    {W, H} = wxFrame:getClientSize(Frame),
    ScaleW = W / Cols,
    ScaleH = H / Rows,
    Coord = {trunc(X / ScaleW), trunc(Y / ScaleH)},
    NewState =
        case Type of
            left_up  -> do_click(Coord, State);
            right_up -> do_mark(Coord, State)
        end,
    {noreply, NewState};
handle_event(#wx{event = #wxPaint{}}, State) ->
    ok = redraw(State),
    {noreply, State};
handle_event(#wx{id = ID, event = #wxCommand{type = command_menu_selected}},
             State = #s{frame = Frame}) ->
    ok =
        case ID of
            ?gameNEW    -> ms_con:new_game();
            ?gameETC    -> do_settings(Frame);
            ?wxID_EXIT  -> close(Frame);
            ?wxID_ABOUT -> show_about(Frame)
        end,
    {noreply, State};
handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = close(Frame),
    {noreply, State};
handle_event(Event, State) ->
    ok = tell(info, "Unexpected event ~tp~n", [Event]),
    {noreply, State}.


code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().


close(Frame) ->
    ok = ms_con:stop(),
    wxFrame:destroy(Frame).



%%% Doers

do_click(Coord, State = #s{squares = Squares}) ->
    ok =
        case lists:keyfind(Coord, #square.coord, Squares) of
            #square{status = unclicked} -> ms_con:clicked(Coord);
            _                           -> ok
        end,
    State.


do_mark(Coord, State = #s{squares = Squares, flag_count = Flags}) ->
    case lists:keyfind(Coord, #square.coord, Squares) of
        Selected = #square{status = unclicked} ->
            Updated = Selected#square{status = marked},
            NewSquares = lists:keystore(Coord, #square.coord, Squares, Updated),
            NewState = State#s{squares = NewSquares, flag_count = Flags + 1},
            ok = redraw(NewState),
            ok = update_status(NewState),
            NewState;
        Selected = #square{status = marked} ->
            Updated = Selected#square{status = maybe},
            NewSquares = lists:keystore(Coord, #square.coord, Squares, Updated),
            NewState = State#s{squares = NewSquares, flag_count = Flags - 1},
            ok = redraw(NewState),
            ok = update_status(NewState),
            NewState;
        Selected = #square{status = maybe} ->
            Updated = Selected#square{status = unclicked},
            NewSquares = lists:keystore(Coord, #square.coord, Squares, Updated),
            NewState = State#s{squares = NewSquares},
            ok = redraw(NewState),
            NewState;
        _ ->
            State
    end.


do_mine_total(Count, State) ->
    StartTS = os:timestamp(),
    Timer = erlang:send_after(300, self(), time_hack),
    NewState = State#s{start_ts = StartTS, timer = Timer, mine_total = Count},
    ok = update_status(State),
    NewState.


do_time_hack(State = #s{timer = none}) ->
    ok = update_status(State),
    State;
do_time_hack(State) ->
    NewTimer = erlang:send_after(300, self(), time_hack),
    ok = update_status(State),
    State#s{timer = NewTimer}.


update_status(#s{frame = Frame, start_ts = none}) ->
    Flags = 0,
    Mines = 0,
    Seconds = 0.0,
    F = io_lib:format("~3.3.0w / ~3.3.0w  |  ~7.3.0f", [Flags, Mines, Seconds]),
    Text = unicode:characters_to_list(F),
    wxFrame:setStatusText(Frame, Text);
update_status(#s{frame      = Frame,
                 start_ts   = TS,
                 flag_count = Flags,
                 mine_total = Mines}) ->
    Now = os:timestamp(),
    Seconds = timer:now_diff(Now, TS) / 1000000,
    F = io_lib:format("~3.3.0w / ~3.3.0w  |  ~7.3.0f", [Flags, Mines, Seconds]),
    Text = unicode:characters_to_list(F),
    wxFrame:setStatusText(Frame, Text).


do_update(Board, State = #s{squares = Squares, flag_count = Flags}) ->
    {NewSquares, NewFlags} = update_squares(Board, Squares, Flags),
    NewState = State#s{squares = NewSquares, flag_count = NewFlags},
    ok = redraw(NewState),
    ok = update_status(NewState),
    NewState.

update_squares([{Coord, Status} | Rest], Squares, Flags) ->
    Selected = lists:keyfind(Coord, #square.coord, Squares),
    Updated = Selected#square{status = Status},
    NewSquares = lists:keystore(Coord, #square.coord, Squares, Updated),
    NewFlags =
        case Selected#square.status == marked of
            true  -> Flags - 1;
            false -> Flags
        end,
    update_squares(Rest, NewSquares, NewFlags);
update_squares([], Squares, Flags) ->
    {Squares, Flags}.


do_new_grid(X, Y, State = #s{frame = Frame}) ->
    BoardPX = {X * 30, Y * 30},
    ok = wxFrame:setClientSize(Frame, BoardPX),
    ok = wxFrame:center(Frame),
    Squares = gen_squares(X, Y, 0, []),
    NewState = State#s{start_ts   = none,
                       mine_total = 0,
                       flag_count = 0,
                       squares    = Squares,
                       size       = {X, Y}},
    ok = update_status(NewState),
    ok = redraw(NewState),
    NewState.

gen_squares(MaxX, MaxY, X, Rows) when X < MaxX ->
    NewRow = gen_row(X, MaxY, 0, []),
    gen_squares(MaxX, MaxY, X + 1, [NewRow | Rows]);
gen_squares(_, _, _, Rows) ->
    lists:append(lists:reverse(Rows)).

gen_row(X, MaxY, Y, Squares) when Y < MaxY ->
    NewSquares = [#square{coord = {X, Y}} | Squares],
    gen_row(X, MaxY, Y + 1, NewSquares);
gen_row(_, _, _, Squares) ->
    lists:reverse(Squares).


redraw(#s{frame     = Frame,
          panel     = Panel,
          size      = {Cols, Rows},
          squares   = Squares,
          unclicked = Unclicked,
          empty     = Empty,
          marked    = Marked,
          maybe     = Maybe,
          mine      = Mine,
          assplode  = Assplode,
          adjacent  = Adjacent}) ->
    {W, H} = wxFrame:getClientSize(Frame),
    ScaleW = W / (Cols * 30),
    ScaleH = H / (Rows * 30),
    DC = wxClientDC:new(Panel),
    ok = wxClientDC:setUserScale(DC, ScaleW, ScaleH),
    Draw =
        fun(#square{coord = {LX, LY}, status = Status}) ->
            Image =
                case Status of
                    unclicked     -> Unclicked;
                    empty         -> Empty;
                    {adjacent, N} -> element(N, Adjacent);
                    marked        -> Marked;
                    maybe         -> Maybe;
                    mine          -> Mine;
                    assplode      -> Assplode
                end,
            wxClientDC:drawBitmap(DC, Image, {LX * 30, LY * 30})
        end,
    ok = lists:foreach(Draw, Squares),
    wxClientDC:destroy(DC).


funeral(State = #s{frame = Frame, timer = Timer}) ->
    _ = erlang:cancel_timer(Timer),
    ok = timer:sleep(500),
    Message = "YOUR HEAD ASSPLODE!",
    Options = [{caption, "LOSE!"}, {style, ?wxICON_EXCLAMATION}],
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    ok = wxMessageDialog:destroy(Dialog),
    State#s{timer = none}.


party(State = #s{frame = Frame, timer = Timer}) ->
    _ = erlang:cancel_timer(Timer),
    ok = timer:sleep(500),
    Message = "AWESOME!",
    Options = [{caption, "WIN!"}, {style, ?wxICON_EXCLAMATION}],
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    ok = wxMessageDialog:destroy(Dialog),
    State#s{timer = none}.


do_settings(Frame) ->
    {{X, Y}, Density} = ms_con:get_conf(),
    MinX = 6,
    MaxX = 100,
    MinY = 6,
    MaxY = 60,
    D = trunc(Density * 100),
    MinD = 1,
    MaxD = 99,

    Dialog = wxDialog:new(Frame, ?wxID_ANY, "Settings", [{size, {400, 400}}]),
    Sizer = wxBoxSizer:new(?wxVERTICAL),

    Wide = [{flag, ?wxEXPAND}, {proportion, 1}],

    Style = [{style, ?wxSL_HORIZONTAL bor ?wxSL_LABELS}],
    SliderX = wxSlider:new(Dialog, ?wxID_ANY, X, MinX, MaxX, Style),
    SzX = wxStaticBoxSizer:new(?wxHORIZONTAL, Dialog, [{label, "Grid Width"}]),
    _ = wxSizer:add(SzX, SliderX, Wide),
    SliderY = wxSlider:new(Dialog, ?wxID_ANY, Y, MinY, MaxY, Style),
    SzY = wxStaticBoxSizer:new(?wxHORIZONTAL, Dialog, [{label, "Grid Height"}]),
    _ = wxSizer:add(SzY, SliderY, Wide),
    SliderD = wxSlider:new(Dialog, ?wxID_ANY, D, MinD, MaxD, Style),
    SzD = wxStaticBoxSizer:new(?wxHORIZONTAL, Dialog, [{label, "Mine Density"}]),
    _ = wxSizer:add(SzD, SliderD, Wide),
    ButtonSz = wxDialog:createButtonSizer(Dialog, ?wxOK),

    _ = wxSizer:add(Sizer, SzX, Wide),
    _ = wxSizer:add(Sizer, SzY, Wide),
    _ = wxSizer:add(Sizer, SzD, Wide),
    _ = wxSizer:add(Sizer, ButtonSz, Wide),

    ok = wxDialog:setSizer(Dialog, Sizer),
    
    _ = wxDialog:showModal(Dialog),
    NewX = wxSlider:getValue(SliderX),
    NewY = wxSlider:getValue(SliderY),
    NewD = wxSlider:getValue(SliderD) / 100,
    ok = wxDialog:destroy(Dialog),
    ms_con:set_conf({NewX, NewY}, NewD).


show_about(Frame) ->
    Version = proplists:get_value(vsn, module_info(attributes)),
    Format =
        "Minesweeper v~s\n"
        "A minesweeper clone, in Erlang\n"
        "Author: (c) 2020 Craig Everett <zxq9@zxq9.com>\n"
        "License: MIT (open source; free for any use)\n"
        "Repo URL: https://gitlab.com/zxq9/minesweeper\n"
        "Zomp package ID: otpr-minesweeper",
    Message = io_lib:format(Format, [Version]),
    Options = [{caption, "About"}, {style, ?wxICON_INFORMATION}],
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    wxMessageDialog:destroy(Dialog).
